package com.ilic.textrecognition

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.splash_activity.*

/**
 * Created by Aleksandar Ilić on 30.10.20.
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)

        val animation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        animation.duration = 1000
        animation.repeatCount = 2
        animText.startAnimation(animation)

        Handler(Looper.myLooper()!!).postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
        }, 1500)
    }
}