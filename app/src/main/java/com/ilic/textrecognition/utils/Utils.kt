package com.ilic.textrecognition.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.os.Build
import android.util.Log
import android.widget.Toast
import com.ilic.textrecognition.R
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Aleksandar Ilić on 4.11.20.
 */

fun createPDF(txt: String, context: Context) {
    val pdfDoc = PdfDocument()
    val paint = Paint()
    val bodyPaint = Paint()
    val pageWidth = 1200
    val pageHeight = 2010
    val pageInfo: PdfDocument.PageInfo = PdfDocument.PageInfo.Builder(pageWidth, pageHeight, 1).create()
    val myPage = pdfDoc.startPage(pageInfo)
    val canvas = myPage.canvas
    val btmp = BitmapFactory.decodeResource(context.resources, R.drawable.report)
    val scaleBtmp = Bitmap.createScaledBitmap(btmp, 1200, 400, false)
    canvas.drawBitmap(scaleBtmp, 0f, 0f, paint)

    val c = 25f
    var y = 730f
    paint.textAlign = Paint.Align.LEFT
    paint.textSize = 35f
    val dateTime = SimpleDateFormat("yyyy-MM-dd HH-mm", Locale.US)
    canvas.drawText("PDF creator: ${Build.MODEL}", 20f, 540f, paint)
    canvas.drawText("Date/Time created: ${dateTime.format(System.currentTimeMillis())}", 20f, 575f, paint)

    paint.textAlign = Paint.Align.RIGHT
    paint.textSize = 35f
    val random = Random()
    val randomNum = random.nextInt(1000 - 1) + 1

    canvas.drawText("Report number: $randomNum", pageWidth - 20f, 540f, paint)

    canvas.drawRect(25f, 590f, pageWidth - 25f, 595f, paint)
    bodyPaint.textSize = 35f
    for (x: String in txt.split("\n")) {
        canvas.drawText(x, c, y, bodyPaint)
        y+=bodyPaint.descent() - bodyPaint.ascent()
    }
    pdfDoc.finishPage(myPage)

    val file = File(context.getExternalFilesDir("Reports"), dateTime.format(System.currentTimeMillis()) + ".pdf")

    try {
        pdfDoc.writeTo(FileOutputStream(file))
        Toast.makeText(context, "PDF created: ${file.absoluteFile}", Toast.LENGTH_LONG).show()
    } catch (ex: Exception) {
        Log.e("pdf",ex.message.toString())
    }
    pdfDoc.close()
}
