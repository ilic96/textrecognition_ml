package com.ilic.textrecognition

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.util.valueIterator
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.TextRecognizer
import com.ilic.textrecognition.utils.createPDF
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var uri: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.addImage -> showImageImportDialog()
            R.id.createPDF -> createPDF(resultText.text.toString(), this@MainActivity)
        }
       return super.onOptionsItemSelected(item)
    }

    private fun showImageImportDialog() {
        val items = resources.getStringArray(R.array.alert_items)
        val dialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.DarkAlertDialog)).apply {
            setTitle("Add image")
            setItems(items) { _, option ->
                if (option == 0) {
                    if (!checkPermissionCamera()) {
                        requestPermissionCamera()
                    } else {
                        pickCamera()
                    }
                }
                if (option == 1) {
                    if (!checkPermissionStorage()) {
                        requestPermissionStorage()
                    } else {
                        pickGallery()
                    }
                }
            }
        }
        dialog.create().show()
    }

    private fun pickGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE)
    }

    private fun pickCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "NewRecord")
        values.put(MediaStore.Images.Media.DESCRIPTION, "Image to text")
        uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, IMAGE_PICK_CAMERA_CODE)
    }

    private fun requestPermissionStorage() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            STORAGE_REQUEST_CODE
        )
    }

    private fun checkPermissionStorage(): Boolean {
        val cond =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return cond == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissionCamera() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
            CAMERA_REQUEST_CODE
        )
    }

    private fun checkPermissionCamera(): Boolean {
        val cond = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        val cond1 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        return cond && cond1
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    val cond = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    if (cond) {
                        pickCamera()
                    } else {
                        Toast.makeText(this, "Camera permission denied!", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            STORAGE_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    val cond = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    if (cond) {
                        pickGallery()
                    } else {
                        Toast.makeText(this, "Storage permission denied!", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_PICK_CAMERA_CODE) {
                CropImage.activity(uri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
            }
            if (requestCode == IMAGE_PICK_GALLERY_CODE) {
                CropImage.activity(data?.data)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                val resultUri = result.uri
                imagePreview.setImageURI(resultUri)

                val bitmapDrawable = imagePreview.drawable
                val bitmap = (bitmapDrawable as BitmapDrawable).bitmap
                val recognizer = TextRecognizer.Builder(application).build()
                if (!recognizer.isOperational) {
                    Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                    finish()
                }
                    val frame = Frame.Builder().setBitmap(bitmap).build()
                    val items = recognizer.detect(frame)
                    val sb = StringBuilder()

                    var i = 0
                    for (x in items.valueIterator()) {

                        val myItem = items.valueAt(i)
                        sb.append(myItem.value)
                        sb.append("\n")
                        i++
                    }
                    resultText.setText(sb.toString())
                }
        }
    }

    override fun onBackPressed() {
        finishAffinity()
    }
    companion object {
        private const val CAMERA_REQUEST_CODE = 1100
        private const val STORAGE_REQUEST_CODE = 1101
        private const val IMAGE_PICK_GALLERY_CODE = 1200
        private const val IMAGE_PICK_CAMERA_CODE = 1201
    }
}